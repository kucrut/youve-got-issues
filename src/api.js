function getNextPageLink( linkHeader ) {
    const regex = RegExp( '<(.*)>; rel="next"' );
    const links = linkHeader.split( ', ' ) .filter( l => /rel="next"$/.test( l ) );

    if ( ! links.length ) {
        return null;
    }

    const search = regex.exec( links[0] );

    return search[1] || null;
}

export function parseResponse( resp ) {

    return resp.json().then( data => {
        if ( resp.ok ) {
			if ( Array.isArray( data ) && resp.headers.has( 'Link' ) ) {
                data.__nextPageLink = getNextPageLink( resp.headers.get( 'Link' ) );
			}

			return data;
		}

		const err = new Error( data.message || 'Unknown server error' );
		err.code = data.code || '__unknown';
		err.response = resp;
		err.data = data;

        throw err;
	} );
}

export async function fetchRepo( token, repo ) {
    return fetch( `https://api.github.com/repos/${ repo }`, {
        headers: {
            'Authorization': `token ${ token }`,
        }
    } ).then( parseResponse );
}

export async function fetchIssues( token, repo, url ) {
    const finalUrl = url ? url : `https://api.github.com/repos/${ repo }/issues`;

    return fetch( finalUrl, {
        headers: {
            'Authorization': `token ${ token }`,
        }
    } ).then( parseResponse );
}

export async function fetchZenHubIssue( token, repoId, issueNumber ) {
    return fetch( `https://api.zenhub.io/p1/repositories/${ repoId }/issues/${ issueNumber }?access_token=${ token }` )
        .then( parseResponse );
}