import { writable } from 'svelte/store';

const initialIssues = {
    repo: '',
    items: [],
    nextPage: null,
};

const initialFormValues = {
    delimiter: ';',
    ghToken: '',
    zhToken: '',
    repoName: '',
};

function mergeItems( existing, next ) {
	const newIds = next.map( item => item.number );
	const deduplicated = existing.filter( item => newIds.indexOf( item.number ) === -1 );

    return [
		...deduplicated,
		...next,
	];
}

function createIssues( initial = initialIssues ) {
    const { subscribe, set, update } = writable( initial );

	return {
		subscribe,
        reset: () => set( initial ),
        setItems: ( items, nextPage = null ) => update( store => ( {
            ...store,
            items: mergeItems( store.items || [], items ),
            nextPage,
        } ) ),
        setRepo: repo => update( store => ( {
            ...store,
            repo,
        } ) ),
        setZhItem: ( id, item ) => update( store => {
            const { items } = store;
            const ghItem = items.find( i => i.number === id );

            if ( ! ghItem ) {
                return store;
            }

            const index = items.indexOf( ghItem );
            const allItems = [ ...items ];
            allItems[ index ] = {
                ...ghItem,
                ...item,
            };

            return {
                ...store,
                items: allItems,
            };
        } ),
	};
}

function createFormValues( initial = initialFormValues ) {
    const { subscribe, set, update } = writable( initial );

	return {
		subscribe,
        reset: () => set( initial ),
        setValue: ( key, value ) => update( store => ( {
            ...store,
            [ key ]: value,
        } ) ),
        setAll: values => update( store => ( {
            ...store,
            ...values,
        } ) ),
    };
}

export const issues = createIssues();
export const formValues = createFormValues();
