# You've Got Issues!

...

## Get started

Install the dependencies...

```bash
git clone https://gitlab.com/kucrut/youve-got-issues.git
cd youve-got-issues
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running.
